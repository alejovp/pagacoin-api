package com.pagacoin.pagacoinapi.user;

import io.swagger.v3.oas.annotations.Parameter;

import com.pagacoin.pagacoinapi.wallet.Wallet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@CrossOrigin
@RequestMapping(value = "user")
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);
    
    private final UserRepository userRepository;

    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/")
    @PageableAsQueryParam
    public Page<User> findAllUsers(@Parameter(hidden = true) Pageable pageable) {
        logger.debug("findAllUsers");

        Page<User> result = this.userRepository.findAll(pageable);
        result.forEach(this::calculateBalance);

        return result;
    }

    @GetMapping("/{id}")
    public User getUserById(@PathVariable Long id) {
        logger.debug("getUserById: {}", id);

        User user = this.userRepository.findById(id)
                        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        
        calculateBalance(user);

        return user;
    }

    private void calculateBalance(User user) {
        logger.debug("calculateBalance");

        Long sum = user.getWallets().stream().mapToLong(Wallet::getBalance).sum();
        user.setTotalBalance(sum);
    }

}