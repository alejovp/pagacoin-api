package com.pagacoin.pagacoinapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PagacoinApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PagacoinApiApplication.class, args);
	}

}
