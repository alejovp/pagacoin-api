package com.pagacoin.pagacoinapi.wallet;

import lombok.Data;

@Data
public class Transfer {
    private String hashFrom;
    private String hashTo;
    private Integer amount;
}
