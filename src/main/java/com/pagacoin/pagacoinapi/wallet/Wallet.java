package com.pagacoin.pagacoinapi.wallet;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "WALLET")
public class Wallet {

    @Id
    @Column(name = "WALLET_ID")
    private Integer walletId;

    @Column(name = "HASH")
    private String hash;

    @Column(name = "BALANCE")
    private Integer balance;

    @Column(name = "USER_ID")
    private Long userId;

}