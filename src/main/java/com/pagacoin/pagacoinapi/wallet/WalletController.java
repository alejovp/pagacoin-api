package com.pagacoin.pagacoinapi.wallet;

import io.swagger.v3.oas.annotations.Parameter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;


@RestController
@CrossOrigin
@RequestMapping(value = "wallet")
public class WalletController {
    
    private static final Logger logger = LoggerFactory.getLogger(WalletController.class);

    private final WalletRepository walletRepository;

    public WalletController(WalletRepository walletRepository) {
        this.walletRepository = walletRepository;
    }

    @GetMapping("/")
    @PageableAsQueryParam
    public Page<Wallet> findAllWallets(@Parameter(hidden = true) Pageable pageable) {
        return this.walletRepository.findAll(pageable);
    }

    @PostMapping("/transfer")
    public ResponseEntity<String> transferBalance(@RequestBody Transfer transfer) {
        logger.debug("transferBalance");

        Wallet walletFrom = getWalletFromHashForTransfer(transfer.getHashFrom());
        transferBalanceValidator(walletFrom, transfer.getAmount());
        walletFrom.setBalance(walletFrom.getBalance() - transfer.getAmount());

        Wallet walletTo = getWalletFromHashForTransfer(transfer.getHashTo());
        walletTo.setBalance(walletTo.getBalance() + transfer.getAmount());

        walletRepository.save(walletFrom);
        walletRepository.save(walletTo);

        return new ResponseEntity<>("Transfer success :)", HttpStatus.OK);
    }

    private Wallet getWalletFromHashForTransfer(String hash) throws ResponseStatusException {
        logger.debug("getWalletFromHashForTransfer");

        Wallet wallet = this.walletRepository.findByHash(hash);
        if (wallet == null) {
            logger.error("Hash {} not found in data base", hash);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return wallet;
    }

    private void transferBalanceValidator(Wallet wallet, Integer amount) throws ResponseStatusException {
        logger.debug("transferBalanceValidator");

        if ((wallet.getBalance() - amount) < 0) {
            logger.error("Not enough balance in hash: {}", amount);
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Not enough balance :(");
        }
    }

}