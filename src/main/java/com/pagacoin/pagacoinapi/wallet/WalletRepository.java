package com.pagacoin.pagacoinapi.wallet;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WalletRepository extends PagingAndSortingRepository<Wallet, Integer> {

    Wallet findByHash(String hash);

}