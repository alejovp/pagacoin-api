# PagaCoin Api #

Very basic Rest API created with Srping Boot, java 11 and open api for demo purposes.

## Requirements:
* [Download and install Maven](https://maven.apache.org/download.cgi)
* [Download an install Java 11](https://jdk.java.net/archive/)

## Configuration properties:
* [application.yml](config/application.properties)

## Dependencies file:
* [pom.xml](pom.xml)

## Build project:
* [mvn clean install](bitbucket-pipelines.yml)

## Run project:
* java -jar [pagacoin-api-1.0.0-SNAPSHOT.jar](target/pagacoin-api-1.0.0-SNAPSHOT.jar)

## API specification: 
* [Open API definition](http://localhost:8080/pagacoin/api/swagger-ui.html)